Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bigmemory
Upstream-Contact: Michael J. Kane <bigmemoryauthors@gmail.com>
Source: https://cran.r-project.org/package=bigmemory

Files: *
Copyright: 2008-2018 Michael J. Kane <kaneplusplus@gmail.com>, John W. Emerson <jayemerson@gmail.com>, Peter Haverty <haverty.peter@gene.com>, and Charles Determan Jr. <cdetermanjr@gmail.com>
License: LGPL-3 or Apache-2

Files: debian/*
Copyright: 2018 Dylan Aïssi <bob.dybian@gmail.com>
License: LGPL-3 or Apache-2

License: LGPL-3
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 in version 3 of the license as published by the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3"

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 implied. See the License for the specific language governing
 .
 On Debian systems, the complete text of the Apache
 License, version 2.0, can be found in the file
 `/usr/share/common-licenses/Apache-2.0'.
